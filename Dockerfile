FROM node:current-alpine AS base
WORKDIR /base
COPY ["package.json", "package-lock.json*", "npm-shrinkwrap.json*", "./"]
RUN npm install --production --silent
COPY . .

FROM base AS build
ENV NODE_ENV=production
WORKDIR /build
COPY --from=base /base ./
RUN npm run build && mv node_modules ../
# remove development dependencies
RUN npm prune --production

FROM node:current-alpine AS production
ENV NODE_ENV=production
WORKDIR /app
COPY --from=build /build/package*.json ./
COPY --from=build /build/.next ./.next
RUN npm install next --production --silent

EXPOSE 3000
CMD npm run start
