requires

- WSL (ubuntu)
  - nodejs
    sudo apt update
    sudo apt install nodejs
  - react
  - Next JS
  - Webpack

## Project setting

npm init -y
npm install --save react react-dom next

## Docker

docker build --rm --pull --no-cache --squash -f Dockerfile --label com.microsoft.created-by=visual-studio-code -t hellonext:latest .
docker run -it -d -p 63000:3000 hellonext:latest -name hellonext
docker exec -it hellonext /bin/sh
